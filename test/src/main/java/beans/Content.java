package beans;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the content database table.
 * 
 */
@Entity
@NamedQuery(name="Content.findAll", query="SELECT c FROM Content c")
public class Content implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue (strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	private String q1;

	@Lob
	private String q2;

	@Lob
	private String q3;

	@Lob
	private String q4;

	@Lob
	private String q5;

	@Lob
	private String q6;

	@Lob
	private String q7;
	

	public Content() {
	}

	public Content(String q12, String q22, String q32, String q42, String q52, String q62, String q72) {
		// TODO Auto-generated constructor stub
		this.q1= q12;
		this.q2= q22;
		this.q3= q32;
		this.q4= q42;
		this.q5= q52;
		this.q6= q62;
		this.q7= q72;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQ1() {
		return this.q1;
	}

	public void setQ1(String q1) {
		this.q1 = q1;
	}

	public String getQ2() {
		return this.q2;
	}

	public void setQ2(String q2) {
		this.q2 = q2;
	}

	public String getQ3() {
		return this.q3;
	}

	public void setQ3(String q3) {
		this.q3 = q3;
	}

	public String getQ4() {
		return this.q4;
	}

	public void setQ4(String q4) {
		this.q4 = q4;
	}

	public String getQ5() {
		return this.q5;
	}

	public void setQ5(String q5) {
		this.q5 = q5;
	}

	public String getQ6() {
		return this.q6;
	}

	public void setQ6(String q6) {
		this.q6 = q6;
	}

	public String getQ7() {
		return this.q7;
	}

	public void setQ7(String q7) {
		this.q7 = q7;
	}

}