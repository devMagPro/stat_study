package manageBean;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import beans.Administrator;
import dao.AdminDao;

/**
 * use to manage the bean Administrator
 **/

@ManagedBean(name="adminMB")
@ViewScoped
@SessionScoped
public class AdminMB {

	private Administrator admin = new Administrator();

	private AdminDao adminDao = new AdminDao();
	private List<Administrator> listAdmin;
	
	/**
	 * when call try to log the administrator 
	 * @return String url redirection
	 **/
	public String login() {
		this.listAdmin = adminDao.Login(this.admin);
		if (this.listAdmin.size() > 0) {
			System.out.println("Log in conneted youpi aaa");
			return "Administrator/DashboardAdmin.xhtml?faces-redirect=true";
		} else {
			FacesContext.getCurrentInstance().addMessage("information",
					new FacesMessage("error", "Invalid credential"));
			return "index.xhtml?faces-redirect=true";
		}

	}

	public Administrator getAdmin() {
		return admin;
	}

	public void setAdmin(Administrator admin) {
		this.admin = admin;
	}

	public AdminDao getAdminDao() {
		return adminDao;
	}

	public void setAdminDao(AdminDao adminDao) {
		this.adminDao = adminDao;
	}

	public List<Administrator> getListStudent() {
		return listAdmin;
	}

	public void setListStudent(List<Administrator> listAdmin) {
		this.listAdmin = listAdmin;
	}
}
