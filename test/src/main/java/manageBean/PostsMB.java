package manageBean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import beans.Answer;
import beans.Content;
import beans.Student;
import dao.PostDao;
import dao.StudentDao;

/**
 * class use to manage the bean Answer
 **/

@ManagedBean(name = "postsMB")
@ViewScoped
@SessionScoped
public class PostsMB {

	private PostDao postDao = new PostDao();
	private Answer post = new Answer();
	private List<Answer> listPosts;
	private Content content;
	
	private String q1;
	private String q2;
	private String q3;
	private String q4;
	private String q5;
	private String q6;
	private String q7;

	/**
	 * Save the answer of the user connected to the Answers's table
	 * 
	 * @return String url for redirection
	 **/
	public String Save() {
		System.out.println("Try to save a post");
		//get the id from the session
		Object idStudent = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("idStudent");
		
		Student student = new Student();
		student = new StudentDao().getStudentById(student, idStudent);
		
		this.content = new Content(q1,q2,q3,q4,q5,q6,q7);
		
		System.out.println("attribut set successfully");
		this.post.setContent(this.content);
		this.post.setStudent(student);
		
		this.postDao.Save(this.post);

		//FacesContext context = FacesContext.getCurrentInstance();
		//context.addMessage(null, new FacesMessage("Save Successfully."));

		System.out.println("Save Successfully ban ban ");
		return "Student/DashboardStudent.xhtml?faces-redirect=true";
		//return "Student/DashboardStudent.xhtml?faces-redirect=true";
	}

	/**
	 * use a initialize a list of answers
	 * 
	 * @return List<Answer> list of all posts by student
	 **/
	public void initDataTable() {
		listPosts = postDao.all();
	}

	/**
	 * @return the list of Answer
	 **/
	public List<Answer> getListPosts() {
		return listPosts;
	}

	/**
	 * Set the list of answer 
	 * @param List<Answer> list of answer
	 **/
	public void setListPosts(List<Answer> listPosts) {
		this.listPosts = listPosts;
	}

	/**
	 * get the Answer
	 * @return Answer to save on the database
	 **/
	public Answer getPost() {
		return post;
	}

	/**
	 * set the answer to be save on the database
	 **/
	public void setPost(Answer post) {
		this.post = post;
	}

	public PostDao getPostDao() {
		return postDao;
	}

	public void setPostDao(PostDao postDao) {
		this.postDao = postDao;
	}
	public String getQ1() {
		return q1;
	}

	public void setQ1(String q1) {
		this.q1 = q1;
	}

	public String getQ2() {
		return q2;
	}

	public void setQ2(String q2) {
		this.q2 = q2;
	}

	public String getQ3() {
		return q3;
	}

	public void setQ3(String q3) {
		this.q3 = q3;
	}

	public String getQ4() {
		return q4;
	}

	public void setQ4(String q4) {
		this.q4 = q4;
	}

	public String getQ5() {
		return q5;
	}

	public void setQ5(String q5) {
		this.q5 = q5;
	}

	public String getQ6() {
		return q6;
	}

	public void setQ6(String q6) {
		this.q6 = q6;
	}

	public String getQ7() {
		return q7;
	}

	public void setQ7(String q7) {
		this.q7 = q7;
	}
}
