package dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import beans.Student;
import util.JPAUtil;

/**
 * use for the persistence of the object Student in the database
 **/
public class StudentDao implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private EntityManager entityManager = JPAUtil.getEntityManager("Project_JSF_MYSQL_TOMCAT");

	/**
	 * try to get all student with the given information
	 * 
	 * @param Student student we want to login
	 * @return List<Student> List of student
	 **/
	public List<Student> Login(Student student) {
		String query = "select c from Student c where c.email='" + student.getEmail() + "' and c.password='"
				+ student.getPassword() + "'";
		List<Student> stud = entityManager.createQuery(query).getResultList();

		return stud;
	}
	
	/**
	 * persist the object Student on the database
	 * @param Student object to persist on the database
	 **/
	public void Save(Student student) {
		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();
		entityManager.persist(student);
		tx.commit();
	}


	/**
	 * get a Specific student from the database
	 * @param Student object and id of the student
	 * @return Object Student
	 **/
	public Student getStudentById(Student c, Object id) {
		return entityManager.find(c.getClass(), id);
	}

}
