package dao;

import java.util.List;

import javax.persistence.EntityManager;

import beans.Administrator;
import util.JPAUtil;

/**
 * use for persistence of object Administrator in the database
 **/
public class AdminDao {
	private EntityManager entityManager = JPAUtil.getEntityManager("Project_JSF_MYSQL_TOMCAT");

	/**
	 * @return List<Administrator> the information of the Administrator take in
	 *         parameter
	 * @param Administrator information of administrator to login
	 **/
	public List<Administrator> Login(Administrator admin) {
		String query = "select c from Administrator c where c.email='" + admin.getEmail() + "' and c.password='"
				+ admin.getPassword() + "'";
		List<Administrator> admins = entityManager.createQuery(query).getResultList();

		return admins;
	}
}
