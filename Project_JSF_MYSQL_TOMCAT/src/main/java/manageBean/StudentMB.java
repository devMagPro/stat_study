package manageBean;

import java.io.Serializable;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import beans.Student;
import dao.StudentDao;

import javax.faces.bean.ViewScoped;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.sun.mail.util.MailSSLSocketFactory;

/**
 * use to manage the bean Student
 **/

@ManagedBean(name = "studentMB")
@SessionScoped
@ViewScoped
public class StudentMB implements Serializable {

	private static final long serialVersionUID = 1L;

	private Student student = new Student();
	private StudentDao studentDao = new StudentDao();
	private List<Student> listStudent;
	
	/**logout the connected student
	 * @return String
	 */
	public String logout() {
		this.student.setEmail("");
		this.student.setPassword("");
		
		//invalidate the session
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "../index.xhtml?faces-redirect=true";
	}

	/**
	 * try to login the Student if success save his id on the session
	 * 
	 * @return String url for redirection
	 **/

	public String login() {
		
		this.listStudent = studentDao.Login(this.student);

		if (this.listStudent.size() > 0) {

			this.student = this.listStudent.get(0);
			System.out.println("you are log in as a student");
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("idStudent",
					this.student.getId());

			this.student.setEmail("");
			this.student.setPassword("");
			return "Student/DashboardStudent?faces-redirect=true";
		} else {
			System.out.println("Invalid credential");
			FacesContext.getCurrentInstance().addMessage("information",
					new FacesMessage("error", "Invalid credential"));
			return "index?faces-redirect=true";
		}
	}

	/**
	 * send a email to a student email of the school email = "placerejectorstudy@gmail.com"
	 * 												 password = "sapinsapin"
	 **/
	public String sendMail() {

		System.out.println("email can be send now ");

		// get the email of the student
		// String emailStudent = this.student.getEmail();

		String emailStudent = this.student.getEmail();

		// set the university email
		String emailUniversity = "placerejectorstudy@gmail.com";
		String passwordgmail = "sapinsapin";
		// Get system properties
		Properties properties = System.getProperties();
		MailSSLSocketFactory sf;
		try {
			sf = new MailSSLSocketFactory();
			sf.setTrustAllHosts(true);
			properties.put("mail.smtp.ssl.socketFactory", sf);
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Setup mail server
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "465");
		properties.put("mail.smtp.ssl.enable", "true");
		properties.put("mail.smtp.auth", "true");

		properties.put("mail.smtp.ssl.trust", "*");

		// Get the Session object. and pass username and password
		Session session = Session.getInstance(properties, new javax.mail.Authenticator() {

			protected PasswordAuthentication getPasswordAuthentication() {

				return new PasswordAuthentication(emailUniversity, passwordgmail);

			}

		});

		// Used to debug SMTP issues
		session.setDebug(true);

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(emailUniversity));

			// Set To: header field of the header.
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailStudent));

			// build the mail information
			String password = "";
			Random rd = new Random();
			for (int i = 0; i < 8; i++) {
				char c = (char) (rd.nextInt(26) + 97);
				password += c;
			}

			// Set Subject: header field
			message.setSubject("Study Place Rejection");

			message.setContent(
					"<p> On the following page we would like to ask you a few questions about your current study "
							+ "situation. The goal of our survey is to document your motives for declining the study place in "
							+ "order to create a complete evaluation with the information.</p>"
							+ "<p> You can participate in this survey without giving your name, the data will be fully "
							+ "anonymized and the results will be used solely for the purpose of research.</p>"
							+ "<p> Even in the event of a survey, you have the right to information and deletion of your "
							+ "personal data in compliance with data protection. You can revoke this declaration of consent"
							+ "at any time. </p>"
							+ "<p> In this email, you will find a unique password that you can use with your emai to log in "
							+ "<a href=\"http://localhost:9007/Project_JSF_MYSQL_TOMCAT/faces/index.xhtml\"> here (once)</a>.</p>"
							+ "<p> <strong> password:" + password + " </strong></p>"
							+ "<p> By starting the survey, you agree to have read the above statement in accordance with the"
							+ "data protection for surveys under German law and consent to your data being collected and"
							+ "taken into account for the purpose of this survey.</p>",
					"text/html");

			System.out.println("sending...");

			// Send message
			Transport.send(message);
			System.out.println("Sent message successfully....");

			// save the student information on the database
			Student st = new Student();
			st.setPassword(password);
			st.setEmail(emailStudent);

			(new StudentDao()).Save(st);

		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
		this.student.setEmail("");
		return "DashboardAdmin.xhtml?faces-redirect=true";
	}

	/**
	 * get the current user
	 **/
	public Student getStudent() {
		return student;
	}

	/**
	 * set the current user
	 **/
	public void setStudent(Student student) {
		this.student = student;
	}

	/**
	 * get the list of student select for login
	 * 
	 * @return List<Student>
	 **/
	public List<Student> getListStudent() {
		return listStudent;
	}

	/**
	 * set a list of student for login
	 * 
	 * @param List<Student> list of student
	 **/
	public void setListStudent(List<Student> listStudent) {
		this.listStudent = listStudent;
	}
}
