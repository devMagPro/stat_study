package util;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
/**
 * use for the management of the language of the application
 **/
@ManagedBean(name="changeLocale")
@SessionScoped
public class ChangeLocale implements Serializable {

	/**
	 * default serialize number
	 */
	private static final long serialVersionUID = 1L;
	/*
	 * default local
	 **/
	public String locale = "de";

	/**
	 * default constructor
	 **/
	public ChangeLocale() {
		this.locale="de";
	}
	
	public String hello() {
		System.out.println("je suis present");
		return "";
	}

	/**
	 * change the current language to French 
	 * @return the url for redirection after change the language
	 **/
	public String setAllemand() {
		System.out.println("local change to allemand");
		this.locale = "de";
		return "index.xhtml?faces-redirect=true";
	}

	/**
	 * change the current language to English 
	 * @return the url for redirection after change the language
	 **/
	public String setEnglishLocale() {
		this.locale = "en";
		System.out.println("local change to english");
		return "index.xhtml?faces-redirect=true";
	}

	/**
	 * get the current language
	 **/
	public String getLocale() {
		return this.locale;
	}

}