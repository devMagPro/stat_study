package util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
/**
 * use by JPA for persistence of object
 **/
public class JPAUtil {
	
	private static EntityManagerFactory factory;
	private static EntityManager entityManager;
	
	/**
	 * describe how to manage bean (bean or entity)
	 **/
	public static EntityManager getEntityManager(String persistUnit) {
		
		if (entityManager==null)
		{
		  factory =  Persistence.createEntityManagerFactory(persistUnit);
		
		  entityManager = factory.createEntityManager();
		}
		return entityManager;
	}
}
