package beans;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the answers database table.
 * 
 */
@Entity
@Table(name="answers")
@NamedQuery(name="Answer.findAll", query="SELECT a FROM Answer a")
 
public class Answer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_content", referencedColumnName="id")
	private Content content;
	
	@Temporal(TemporalType.DATE)
	private Date datePost = new Date();
	
	@ManyToOne
	@JoinColumn(name="ID_STUDENT")
	private Student student;

	public Student getStudent() {
		return student;
	}

	public void setDatePost(Date datePost) {
		this.datePost = datePost;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Answer() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Content getContent() {
		return this.content;
	}

	public void setContent(Content content) {
		this.content = content;
	}

	public Date getDatePost() {
		return this.datePost;
	}

	public void setDatePost(Timestamp datePost) {
		this.datePost = datePost;
	}

}