package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import beans.Answer;
import util.JPAUtil;

/**
 * use for persistence of the object Answer in the database
 **/
public class PostDao {
	private EntityManager entityManager = JPAUtil.getEntityManager("Project_JSF_MYSQL_TOMCAT");

	/**
	 * persist the object Answer on the database
	 * @param Answer object to persist on the database
	 **/
	public void Save(Answer post) {
		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();
		entityManager.persist(post);
		tx.commit();
	}

	/**
	 * list of all the Answer in database order by date of creation
	 * 
	 * @return List<Answer> list of Answer
	 **/
	public List<Answer> all() {
		List<Answer> posts = entityManager.createQuery("select c from Answer c ORDER BY c.datePost DESC")
				.getResultList();
		return posts;
	}
}
