-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : Dim 30 mai 2021 à 09:18
-- Version du serveur :  5.7.31
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `answer`
--
CREATE DATABASE IF NOT EXISTS `answer` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `answer`;

-- --------------------------------------------------------

--
-- Structure de la table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
CREATE TABLE IF NOT EXISTS `administrator` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(32) NOT NULL,
  `EMAIL` varchar(32) NOT NULL,
  `PASSWORD` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `administrator`
--

INSERT INTO `administrator` (`ID`, `NAME`, `EMAIL`, `PASSWORD`) VALUES
(1, 'admin', 'admin@admin.com', 'password');

-- --------------------------------------------------------

--
-- Structure de la table `answers`
--

DROP TABLE IF EXISTS `answers`;
CREATE TABLE IF NOT EXISTS `answers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_STUDENT` int(11) NOT NULL,
  `id_content` int(11) NOT NULL,
  `datePost` date DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_ANSWERS_STUDENT` (`ID_STUDENT`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `answers`
--

INSERT INTO `answers` (`ID`, `ID_STUDENT`, `id_content`, `datePost`) VALUES
(5, 1, 5, '2021-05-17'),
(6, 2, 6, '2021-05-17'),
(7, 2, 7, '2021-05-17'),
(8, 1, 8, '2021-05-17'),
(9, 1, 9, '2021-05-17');

-- --------------------------------------------------------

--
-- Structure de la table `content`
--

DROP TABLE IF EXISTS `content`;
CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `q1` text NOT NULL,
  `q2` text NOT NULL,
  `q3` text NOT NULL,
  `q4` text NOT NULL,
  `q5` text NOT NULL,
  `q6` text NOT NULL,
  `q7` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `content`
--

INSERT INTO `content` (`id`, `q1`, `q2`, `q3`, `q4`, `q5`, `q6`, `q7`) VALUES
(5, '4', '5', 'Yes', '2', 'this is the main raison to not thzezr', 'm', 'more than 30'),
(6, '5', '74', 'Yes', '74', 'this is the main raison', 'm', '26-30'),
(7, '4', '5', 'No', '7', 'main while', 'div', '16-20'),
(8, '55', '20', 'Yes', '112', 'this is a content', 'm', '26-30'),
(9, '41', '74', 'No', '74', 'hello this is main raison while', 'f', '21-25');

-- --------------------------------------------------------

--
-- Structure de la table `student`
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(32) NOT NULL,
  `EMAIL` varchar(32) NOT NULL,
  `PASSWORD` char(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `student`
--

INSERT INTO `student` (`ID`, `NAME`, `EMAIL`, `PASSWORD`) VALUES
(1, 'student', 'student@student.com', 'password'),
(2, 'student2', 'student2@student2.com', 'password'),
(3, '', 'arielnana913@gmail.com', 'ucwellnv');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
